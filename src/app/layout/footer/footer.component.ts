import { Component, OnInit } from '@angular/core';
import { ENDERECOS } from './endereco';
import { Endereco } from './endereco-model';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

    constructor() { }

  enderecos: Endereco[] = ENDERECOS;

  public activeEnderecos: any  = {};
      enderecosConfig = {
      slidesToShow: 3,
      slidesToScroll: 1,
      nextArrow: ``,
      prevArrow: ``,
      autoplay: true,
    };

    public idxOn = 0;
    ngOnInit() {}

    public Slide(slide: any) {
      console.log(slide)
      this.activeEnderecos = slide;
    }
}
