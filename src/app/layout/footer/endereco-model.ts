export class Endereco {
    constructor(
       public uf: string,
       public logradouro: string,
       public bairro: string,
       public telefone: string,
       public email: string
    ) {}
}
