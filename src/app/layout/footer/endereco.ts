import { Endereco } from './endereco-model';
export const ENDERECOS: Endereco[] = [
    {
        uf: 'SP',
        logradouro: `Rua Dr. Bacelar, 368 | CJ 14`,
        bairro: `Vila Clementino - São Paulo `,
        telefone: `(11) 2231-6762`,
        email: `contato@agesconsultoria.com.br`
      },
      {
        uf: 'RJ',
        logradouro: `Rua Dr. Bacelar, 368 | CJ 12`,
        bairro: `Vila Clementino - São Paulo `,
        telefone: `(11) 2231-6762`,
        email: `contato@`
      },
      {
        uf: 'MT',
        logradouro: `Rua Dr. Bacelar, 368 | CJ 13`,
        bairro: `Vila Clementino - São Paulo `,
        telefone: `(11) 2231-6762`,
        email: `contato@`
      }
    ];
