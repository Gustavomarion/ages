export const MapaData: any = [
    { uf: 'rr', name: 'Roraima', distributors: [] },
    { uf: 'ap', name: 'Amapá', distributors: [] },
    { uf: 'am', name: 'Amazonas', distributors: []},
    { uf: 'pa', name: 'Pará',  distributors: []},
    { uf: 'ac', name: 'Acre', distributors: []},
    { uf: 'ro', name: 'Rondônia', distributors: []},
    { uf: 'to', name: 'Tocantins', distributors: [] },
    { uf: 'ma', name: 'Maranhão', distributors: []},
    { uf: 'pi', name: 'Piauí', distributors: []},
    { uf: 'ce', name: 'Ceará', distributors: []},
    { uf: 'pb', name: 'Paraíba', distributors: []},
    { uf: 'ba', name: 'Bahia', distributors: []},
    { uf: 'pe', name: 'Pernambuco', distributors: []},
    { uf: 'al', name: 'Alagoas', distributors: []},
    { uf: 'se', name: 'Sergipe', distributors: []},
    { uf: 'mt', name: 'Mato Grosso', distributors: [
        {
            title: 'Mato Grosso',
            logradouro: `Rua Dr. Bacelar, 368 | CJ 14`,
            bairro: `Vila Clementino - São Paulo `,
            telefone: `(11) 2231-6762`,
            email: `contato@agesconsultoria.com.br`,
            horario: `10h às 17h `
        },
    ]},
    { uf: 'go', name: 'Goiás',  distributors: []},
    { uf: 'ms', name: 'Mato Grosso do Sul', distributors: []},
    { uf: 'mg', name: 'Minas Gerais', distributors: [] },
    { uf: 'es', name: 'Espírito Santo', distributors: [] },
    { uf: 'sp', name: 'São Paulo', distributors: [
        {
            title: 'São Paulo',
            logradouro: `Rua Dr. Bacelar, 368 | CJ 14`,
            bairro: `Vila Clementino - São Paulo `,
            telefone: `(11) 2231-6762`,
            email: `contato@agesconsultoria.com.br`,
            horario: `10h às 17h `
        }
    ]},

    { uf: 'rj', name: 'Rio de Janeiro', distributors: [
        {
            title: 'Rio de Janeiro',
            logradouro: `Rua Dr. Bacelar, 368 | CJ 14`,
            bairro: `Vila Clementino - São Paulo `,
            telefone: `(11) 2231-6762`,
            email: `contato@agesconsultoria.com.br`,
            horario: `10h às 17h `
        },
    ]},
    { uf: 'pr', name: 'Paraná',  distributors: []},
    { uf: 'sc', name: 'Santa Catarina', distributors: []},
    { uf: 'rs', name: 'Rio Grande do Sul', distributors: []},
];
