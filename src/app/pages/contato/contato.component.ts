import { Component, OnInit } from '@angular/core';
import { MapaData } from './mapa-data';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-contato',
  templateUrl: './contato.component.html',
  styleUrls: ['./contato.component.scss']
})
export class ContatoComponent implements OnInit {
  public mapaData: any = MapaData;

  public distributors: any = [];
  public filteredDistributors: Observable<any[]>;
  public stateCtrl = new FormControl('');

  constructor() { }

  ngOnInit() {
      this.distributors = [];
      this.mapaData.forEach((e, i) => {
          this.selectDistributors(e.distributors);
      });

      // console.info(this.distributors)
      this.stateCtrl.valueChanges.subscribe((value: string) =>  {
          this.changeDistrubutors( this._filter(value) );
      });
  }

  public changeValueOfSelect(value: string) {
      this.stateCtrl.setValue(value);
  }

  public changeDistrubutors(distributors: any) {
      this.distributors = [];
      return this.selectDistributors(distributors);
  }

  public selectDistributors(distributors: any) {
      for (const distributor of distributors) {
          this.distributors.push(distributor);
      }
  }

  public  _filter(value: string) {
      value = value.toLowerCase();
      // console.log('dsas');
      let distributors = [];
      this.mapaData.map((state: any) => {
          state.selected = false;
          if (state.uf.toLowerCase().includes(value)) {
              // this.changeDistrubutors(state.distributors);
              distributors = distributors.concat(state.distributors);
              state.selected = true;
          }
      });
      // console.log(distributors)

      return distributors;
  }

}
