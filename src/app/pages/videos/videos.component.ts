import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {

  public videos: any[] = [
    {
      id: 1,
      title: 'Título do Vídeo',
      video: './assets/photos/video-1.png',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non placerat',
    },
    {
      id: 2,
      title: 'Título do Vídeo',
      video: './assets/photos/video-2.png',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non placerat',
    },
    {
      id: 3,
      title: 'Título do Vídeo',
      video: './assets/photos/video-3.png',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non placerat',
    },
    {
      id: 4,
      title: 'Título do Vídeo',
      video: './assets/photos/video-1.png',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non placerat',
    },
    {
      id: 5,
      title: 'Título do Vídeo',
      video: './assets/photos/video-2.png',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non placerat',
    },
    {
      id: 6,
      title: 'Título do Vídeo',
      video: './assets/photos/video-3.png',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non placerat',
    },
    {
      id: 7,
      title: 'Título do Vídeo',
      video: './assets/photos/video-1.png',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non placerat',
    },
    {
      id: 8,
      title: 'Título do Vídeo',
      video: './assets/photos/video-2.png',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non placerat',
    },
    {
      id: 9,
      title: 'Título do Vídeo',
      video: './assets/photos/video-3.png',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non placerat',
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
