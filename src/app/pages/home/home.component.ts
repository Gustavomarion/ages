import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public videos: any[] = [
    {
      id: 1,
      title: 'Título do Vídeo',
      video: './assets/photos/video-1.png',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non placerat',
    },
    {
      id: 2,
      title: 'Título do Vídeo',
      video: './assets/photos/video-2.png',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non placerat',
    },
    {
      id: 3,
      title: 'Título do Vídeo',
      video: './assets/photos/video-3.png',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non placerat',
    },
  ];

  public titulo: any[] = [
    {
      id: 1,
      title: 'Eficiência Energética visando redução de custos',
      link: '/solucoes',
    },
    {
      id: 2,
      title: 'Ages',
      link: '/quemsomos',
    },
    {
      id: 3,
      title: 'Confira nossos Cases',
      link: '/cases',
    },
  ];

    constructor() { }
    public activeTitulo: any  = {};
      tituloConfig = {
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: ``,
      prevArrow: ``,
      autoplay: true,
    };

    public Slide(slide: any) {  // metodo
      console.log(slide);
      this.activeTitulo = slide;
  }
    ngOnInit() {
        this.activeTitulo = this.titulo[0];
    }

    teste() {
        console.log('teste');
    }

    slickInit(e) {
        console.log('slick initialized');
    }

    breakpoint(e) {
        console.log('breakpoint');
    }

    afterChange(e) {
        this.activeTitulo = this.titulo[e.currentSlide];
    }

    beforeChange(e) {
    }
}
