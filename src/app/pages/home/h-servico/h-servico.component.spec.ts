import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HServicoComponent } from './h-servico.component';

describe('HServicoComponent', () => {
  let component: HServicoComponent;
  let fixture: ComponentFixture<HServicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HServicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HServicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
