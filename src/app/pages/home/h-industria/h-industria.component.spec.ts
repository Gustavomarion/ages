import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HIndustriaComponent } from './h-industria.component';

describe('HIndustriaComponent', () => {
  let component: HIndustriaComponent;
  let fixture: ComponentFixture<HIndustriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HIndustriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HIndustriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
