import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HServPublicoComponent } from './h-serv-publico.component';

describe('HServPublicoComponent', () => {
  let component: HServPublicoComponent;
  let fixture: ComponentFixture<HServPublicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HServPublicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HServPublicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
