import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HTodosComponent } from './h-todos.component';

describe('HTodosComponent', () => {
  let component: HTodosComponent;
  let fixture: ComponentFixture<HTodosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HTodosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HTodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
