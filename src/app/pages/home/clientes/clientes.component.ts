import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss', '../home.component.scss']
})
export class ClientesComponent implements OnInit {

  constructor() { }

  public clientes: any[] = [
    {
      id: 1,
      logo: './assets/logos/CPFL-Energia-logo.svg'
    },
    {
      id: 2,
      logo: './assets/logos/enel-logo.svg'
    },
    {
      id: 3,
      logo: './assets/logos/light-logo.svg'
    },
    {
      id: 4,
      logo: './assets/logos/edp-logo.svg'
    },
    {
      id: 5,
      logo: './assets/logos/cemig-logo.svg'
    },
    {
      id: 6,
      logo: './assets/logos/logo-energisa.svg'
    },
    {
      id: 7,
      logo: './assets/logos/santa-catarina-logo.svg'
    },
    {
      id: 8,
      logo: './assets/logos/prefeitura-logo.svg'
    },
  ];

  public activeClientes: any  = {};
    clientesConfig = {
      slidesToShow: 7,
      slidesToScroll: 1,
      nextArrow: ``,
      prevArrow: ``,
      autoplay: true,
    };

    public Slide(slide: any) {  // metodo
      console.log(slide);
      this.activeClientes = slide;
  }
    ngOnInit() {
        this.activeClientes = this.clientes[0];
    }

    teste() {
        console.log('teste');
    }

    slickInit(e) {
        console.log('slick initialized');
    }

    breakpoint(e) {
        console.log('breakpoint');
    }

    afterChange(e) {
        this.activeClientes = this.clientes[e.currentSlide];
    }

    beforeChange(e) {
    }

}
