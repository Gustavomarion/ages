import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HComercioComponent } from './h-comercio.component';

describe('HComercioComponent', () => {
  let component: HComercioComponent;
  let fixture: ComponentFixture<HComercioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HComercioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HComercioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
