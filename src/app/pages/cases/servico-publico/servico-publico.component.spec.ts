import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicoPublicoComponent } from './servico-publico.component';

describe('ServicoPublicoComponent', () => {
  let component: ServicoPublicoComponent;
  let fixture: ComponentFixture<ServicoPublicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicoPublicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicoPublicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
