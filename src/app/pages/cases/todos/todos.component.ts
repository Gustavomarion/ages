import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss', '../cases.component.scss']
})
export class TodosComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
