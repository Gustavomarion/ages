import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-solucoes',
  templateUrl: './solucoes.component.html',
  styleUrls: ['./solucoes.component.scss']
})
export class SolucoesComponent implements OnInit {


  constructor(
    private route: ActivatedRoute
  ) { }

  public scroll(id, effect = true): void {
    const el = document.getElementById(id);
    if (effect) {
      el.scrollIntoView({
        behavior: 'smooth'
      });
    } else {
      el.scrollIntoView();
    }
  }

  ngOnInit() {
    //Jquery('body').css('display', 'none');

    this.route.params.subscribe((params: any) => {
      if (params.section) {
        this.scroll(params.section);
      }
    });
  }


}
