import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { QuemsomosComponent } from './pages/quemsomos/quemsomos.component';
import { SolucoesComponent } from './pages/solucoes/solucoes.component';
import { CasesComponent } from './pages/cases/cases.component';
import { VideosComponent } from './pages/videos/videos.component';
import { ContatoComponent } from './pages/contato/contato.component';
import { TodosComponent } from './pages/cases/todos/todos.component';
import { ComercioComponent } from './pages/cases/comercio/comercio.component';
import { ServicoComponent } from './pages/cases/servico/servico.component';
import { IndustriaComponent } from './pages/cases/industria/industria.component';
import { ServicoPublicoComponent } from './pages/cases/servico-publico/servico-publico.component';
import { HTodosComponent } from './pages/home/h-todos/h-todos.component';
import { HComercioComponent } from './pages/home/h-comercio/h-comercio.component';
import { HServicoComponent } from './pages/home/h-servico/h-servico.component';
import { HIndustriaComponent } from './pages/home/h-industria/h-industria.component';
import { HServPublicoComponent } from './pages/home/h-serv-publico/h-serv-publico.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: HomeComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'inicio' },
      { path: 'inicio', component: HTodosComponent },
      { path: 'comercio', component: HComercioComponent },
      { path: 'servico', component: HServicoComponent },
      { path: 'industria', component: HIndustriaComponent },
      { path: 'servico-publico', component: HServPublicoComponent },
    ]
  },
  { path: 'quemsomos', component: QuemsomosComponent },
  { path: 'solucoes', component: SolucoesComponent },
  { path: 'cases', component: CasesComponent,
  children: [
    { path: '', pathMatch: 'full', redirectTo: 'todos' },
    { path: 'todos', component: TodosComponent },
    { path: 'comercio', component: ComercioComponent },
    { path: 'servico', component: ServicoComponent },
    { path: 'industria', component: IndustriaComponent },
    { path: 'servico-publico', component: ServicoPublicoComponent },
  ]
  },
  { path: 'videos', component: VideosComponent },
  { path: 'contato', component: ContatoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
