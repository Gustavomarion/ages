import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { LayoutModule } from '@angular/cdk/layout';
import { HelperService } from 'src/app/services/helper.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './layout/nav/nav.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { QuemsomosComponent } from './pages/quemsomos/quemsomos.component';
import { SolucoesComponent } from './pages/solucoes/solucoes.component';
import { CasesComponent } from './pages/cases/cases.component';
import { VideosComponent } from './pages/videos/videos.component';
import { ContatoComponent } from './pages/contato/contato.component';
import { TodosComponent } from './pages/cases/todos/todos.component';
import { ComercioComponent } from './pages/cases/comercio/comercio.component';
import { ServicoComponent } from './pages/cases/servico/servico.component';
import { IndustriaComponent } from './pages/cases/industria/industria.component';
import { ServicoPublicoComponent } from './pages/cases/servico-publico/servico-publico.component';
import { HTodosComponent } from './pages/home/h-todos/h-todos.component';
import { HComercioComponent } from './pages/home/h-comercio/h-comercio.component';
import { HServicoComponent } from './pages/home/h-servico/h-servico.component';
import { HIndustriaComponent } from './pages/home/h-industria/h-industria.component';
import { HServPublicoComponent } from './pages/home/h-serv-publico/h-serv-publico.component';
import { ClientesComponent } from './pages/home/clientes/clientes.component';



@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    HomeComponent,
    QuemsomosComponent,
    SolucoesComponent,
    CasesComponent,
    VideosComponent,
    ContatoComponent,
    TodosComponent,
    ComercioComponent,
    ServicoComponent,
    IndustriaComponent,
    ServicoPublicoComponent,
    HTodosComponent,
    HComercioComponent,
    HServicoComponent,
    HIndustriaComponent,
    HServPublicoComponent,
    ClientesComponent,
  ],
  imports: [
    BrowserModule,
    SlickCarouselModule,
    LayoutModule,
    AppRoutingModule
  ],
  providers: [ HelperService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
